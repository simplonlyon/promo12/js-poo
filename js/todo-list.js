
class TodoList {

    constructor() {
        /**
         * @type Task[]
         */
        this.tasks = [];
    }
    /**
     * Une méthode permettant de rajouter une Task à la liste des todo
     * @param {string} description la tache à effectuer
     */
    addTask(description) {
        let task = new Task(description);
        this.tasks.push(task);
//        this.tasks.push(new Task(description));
    }
    /**
     * Méthode qui supprimes toutes les tâches qui sont terminées dans la liste
     */
    clearDone() {
        //version filter + fat arrow
        this.tasks = this.tasks.filter(item => !item.done);
        //version filter + fonction anonyme
        // this.tasks = this.tasks.filter(function(item) {
        //     return !item.done;
        // });

        //version avec une boucle classique
        // for (let index = this.tasks.length - 1; index >= 0; index--) {
        //     const task = this.tasks[index];
        //     if(task.done) {
        //         this.tasks.splice(index, 1);
        //     }
            
        // }

    }
    /**
     * Méthode qui génère les éléments html d'une instance de todolist
     * en itérant sur toutes ses tasks
     * @return {Element} Le html de la todo
     */
    draw() {
        //On crée un élément ul qui représentera notre todoList
        let ul = document.createElement('ul');
        //On fait une boucle pour parcourir toutes les tâches dans la todolist
        for (const item of this.tasks) {
            //Pour chaque tâche, on génère son html en utilisant sa méthode draw
            //et on append le html obtenu dans le ul créé juste au dessus
            ul.appendChild(item.draw());
        }
        //On return le ul
        return ul;
    }
}