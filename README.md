# js-poo

## Exo Counter
1. Dans le projet js-poo, créer un fichier exercise-counter.html et exercise-counter.js ainsi qu'un fichier counter.js
2. Dans le HTML, charger d'abord le fichier counter.js puis le fichier exercise-counter.js
3. Dans le fichier counter.js, créer une classe Counter qui aura dans son constructeur une propriété value initialisée à 0 par défaut
4. Dans exercise-counter.js faire une instance du counter et faire un console.log de sa value
5. Dans la classe Counter, rajouter une méthode increment() qui viendra ajouter 1 à la propriété value de la classe
Pour accéder aux propriétés d'une classe, on garde en tête que c'est this.propriete et à partir de là, ça s'utilise comme une variable classique
(On en profite pour tester dans le exercise-counter.js si la méthode marche bien en l'appelant 1 ou 2 fois sur notre instance avant le console log)
6. Faire une seconde méthode decrement() qui fait pareil, mais qui retire 1 à la propriété value puis la méthode reset() qui met cette propriété à zéro
7. Faire une deuxième instance et via des consoles log (ou directement dans la console du navigateur) voir comment se comporte les compteurs l'un par rapport à l'autre (genre si je déclenche le increment() chez l'un, est-ce que ça change la value de l'autre ?)

![diagramme classe counter](./uml/counter.png)

## Exo Todo
### I. La classe Task
1. Créer un fichier exercise-todo.html, exercise-todo.js et task.js
2. Dans task.js, créer une classe Task qui aura une propriété label en string et une propriété done en booléen initialisée à false
3. Ajouter dans la classe Task une méthode toggleDone() qui passera la propriété done de true à false ou de false à true
4. Dans exercise-todo.js faire deux ou trois instances de Task et tester la méthode toggleDone pour voir si elle marche bien

![diagramme classe task](./uml/task.png)

### II. La classe TodoList
1. Créer un nouveau fichier todo-list.js et le charger dans le html (entre task et exercise-todo)
2. Dans ce fichier, créer une classe TodoList qui aura une propriété tasks qui sera initialisée comme un tableau vide
3. Rajouter dans la classe TodoList une méthode addTask qui attendra du string en argument, cette méthode va créer une instance de Task et la push dans la propriété tasks de TodoList
en gros on fait une méthode addTask(text) et dans la méthode on fait un new Task en lui donnant la variable text en argument, puis on fait un push de cette Task sur la propriété this.tasks
4. Faire la méthode clearDone() dans TodoList qui va supprimer de la propriété tasks toutes les tâches qui ont leur done à true (plus d'infos en spoiler)
plusieurs manières possibles, la plus simple est d'utiliser la méthode filter(), chercher Array.filter sur mozilla par exemple pour voir comment l'utiliser. Alternativement, on peut faire une boucle for classique qui parcours this.tasks à l'envers (en partant de la fin pour aller jusqu'à zéro) et dans la boucle, faire un splice si la tâche actuelle a done === true

![diagramme classe task](./uml/todo-list.png)
### III. Generation du HTML de la Task
1. Dans la classe Task, rajouter une méthode draw qui aura pour objectif de créer les éléments html reprénsentant la tâche qui a déclenché la méthode
2. Dans cette méthode, faire en sorte de créer un élément html du type que vous voulez ( un li par exemple), ce sera le contenant de notre tâche
3. Modifier le textContent de cet élément pour lui assigner comme valeur la propriété label de la classe
4. Toujours dans la méthode, faire en sorte de créer un deuxième élément input et changer sa propriété type pour en faire un 'checkbox'
5. Mettre l'élément input dans l'élément fait à l'étape 2, puis faire un return de l'élément en question (li)
6. Pour tester, dans le fichier exercise-todo.js, créer une instance de Task manuellement, puis déclencher sa méthode draw et faire un append du résultat de cette méthode dans le body de notre html
7. Si ça marche, rajouter une condition dans la méthode draw qui mettra la propriété checked de l'input à true si jamais la propriété done de la Task est true
### IV. Génération du HTML de la TodoList
1. Créer une méthode draw dans la classe TodoList
2. Dans cette méthode, créer un élément HTML ul par exemple
3. Ensuite, faire une boucle (for...of) sur toutes les tasks actuellement contenues dans la TodoList
4. A chaque tour de boucle, faire en sorte de lancer la méthode draw() de chaque task et apprend le résultat de cette méthode au ul créé précédemment
5. Faire un return du ul, puis côté exercice-todo.html, faire une instance de TodoList, faire 3-4 addTodo() puis lancer la méthode draw() et append son résultat dans le document
### V. Bonus (solution sur la branche bonus)
Faire en sorte de pouvoir rajouter des tasks et clear notre todolist via le html et que l'affichage se mette à jour.
1. Dans TodoList, rajouter un argument target au constructor et une propriété à laquelle il sera assigné
2. Dans la méthode draw() de la TodoList, faire en sorte de remettre le html de la propriété target à zéro (innerHTML) puis d'appendChild le ul dans ce même élément target
3. A la fin de la méthode addTask et de la méthode clearDone, rajouter un appel à la méthode draw() (pour que l'affichage de la TodoList se mette à jour tout seul)
4. Dans le HTML, rajouter un input et deux button, un add et un clear
5. Dans le exercise-todo.js rajouter un event au click sur le bouton add qui va déclencher un addTask en lui donnant comme valeur la value de l'input
6. Mettre ensuite un event au click sur le bouton clear qui déclenchera la méthode clearDone